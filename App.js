import React, {Component} from 'react';
import OneSignal from 'react-native-onesignal';
import {Provider} from 'react-redux';
import store from './src/store';
import {isSignedIn} from './src/auth';
import {createRootNavigator} from './src/Router';

//TODO: Remove after tests
import {AsyncStorage, StyleSheet} from 'react-native';

export default class App extends Component {
	
	state = {
		signedIn: false,
		checked: false
	};
	
	componentWillMount() {
		
		//TODO: Remove after tests
		AsyncStorage.clear();
		
		isSignedIn()
			.then(res => this.setState({signedIn: res, checked: true}))
			.catch(err => alert('Erro ao autenticar'));
		
		OneSignal.addEventListener('received', this.onReceived);
		OneSignal.addEventListener('opened', this.onOpened);
		OneSignal.addEventListener('registered', this.onRegistered);
		OneSignal.addEventListener('ids', this.onIds);
	}
	
	componentWillUnmount() {
		OneSignal.removeEventListener('received', this.onReceived);
		OneSignal.removeEventListener('opened', this.onOpened);
		OneSignal.removeEventListener('registered', this.onRegistered);
		OneSignal.removeEventListener('ids', this.onIds);
	}
	
	componentDidMount() {
		OneSignal.configure({});
	}
	
	onReceived(notification) {
		console.log("Notification received: ", notification);
	}
	
	onOpened(openResult) {
		console.log('Message: ', openResult.notification.payload.body);
		console.log('Data: ', openResult.notification.payload.additionalData);
		console.log('isActive: ', openResult.notification.isAppInFocus);
		console.log('openResult: ', openResult);
	}
	
	onRegistered(notifData) {
		console.log("Device had been registered for push notifications!", notifData);
	}
	
	onIds(device) {
		console.log('Device info: ', device);
	}
	
	render() {
		
		const {signedIn, checked} = this.state;
		
		if (!checked) return null;
		
		const Layout = createRootNavigator(signedIn);
		
		return (
			<Provider store={store}>
				<Layout style={styles.main}/>
			</Provider>
		);
	}
}

const styles = StyleSheet.create({
	main: {
		flex: 1,
		padding: 20
	}
});
