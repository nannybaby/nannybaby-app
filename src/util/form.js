import Yup from 'yup';
import moment from 'moment/moment';

/**
 * @author Thiago Brezinski
 *
 * Adds 'sameAs' method to Yup for verifying if two or more input fields match.
 *
 */
Yup.addMethod(Yup.mixed, 'sameAs', function(ref, message) {
	return this.test('sameAs', message, function (value) {
		let other = this.resolve(ref);
		
		return !other || !value || value === other;
	})
});

export const loginInitialValues = {
	email: '',
	password: ''
};

export const loginValidationSchema = Yup.object().shape({
	email: Yup.string()
		.email('Digite um e-mail válido')
		.required('Preencha o campo de e-mail'),
	password: Yup.string()
		.min(6, 'A senha deve conter no mínimo 6 caracteres')
		.required('Preencha o campo de senha'),
});

export const signUpInitialValues = {
	email: '',
	password: '',
	passwordConfirmation: '',
	name: '',
	surname: '',
	birthday: new Date(),
};

export const signUpValidationSchema = Yup.object().shape({
	email: Yup.string()
		.email('E-mail inválido')
		.required('É necessário informar um e-mail'),
	password: Yup.string()
		.min(6, 'A senha deve conter no mínimo 6 caracteres')
		.required('É necessário informar uma senha'),
	passwordConfirmation: Yup.string()
		.sameAs(Yup.ref('password'), 'As senhas não correspondem')
		.required('É necessário informar a confirmação de senha'),
	name: Yup.string()
		.min(3, 'O nome deve conter no mínimo 3 caracteres')
		.required('É necessário informar um nome'),
	surname: Yup.string()
		.required('É necessário informar um sobrenome'),
	birthday: Yup.date()
		.max(
			moment().subtract(18, 'years').add(1, 'day').format('YYYY-MM-DD'),
			'Somente maiores de 18 anos podem se cadastrar'
		)
		.required('É obrigatório informar a data de nascimento')
});

export const profileEditValidationSchema = Yup.object().shape({
	name: Yup.string()
		.min(3, 'O nome deve conter no mínimo 3 caracteres')
		.required('É necessário informar um nome'),
	surname: Yup.string()
		.required('É necessário informar um sobrenome'),
	birthday: Yup.date()
		.max(
			moment().subtract(18, 'years').add(1, 'day').format('YYYY-MM-DD'),
			'Somente maiores de 18 anos podem se cadastrar'
		)
		.required('É obrigatório informar a data de nascimento')
});

export const userEditValidationSchema = Yup.object().shape({
	password: Yup.string()
		.min(6, 'A senha deve conter no mínimo 6 caracteres')
		.required('É necessário informar uma senha'),
	passwordConfirmation: Yup.string()
		.sameAs(Yup.ref('password'), 'As senhas não correspondem')
		.required('É necessário informar a confirmação de senha')
});

export const userEditInitialValues = {
	password: '',
	passwordConfirmation: ''
};