import SafariView from 'react-native-safari-view';
import {Linking, Platform} from 'react-native';

/**
 * @author Thiago Brezinski
 *
 * Opens webview and loads some url. In iOS, opens SafariView.
 *
 * @param url url to be loaded
 */
export const openURL = url => {
	if (Platform.OS === 'ios') {
		SafariView.show({
			url: url,
			fromBottom: true,
		});
	} else {
		Linking.openURL(url);
	}
};
