import React from 'react';
import {StackNavigator, TabNavigator} from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
	PreLogin,
	Login,
	Home,
	PreSignUp,
	SignUpBabysitter,
	SignUpParent,
	ProfileEdit,
	ProfileOverview,
	UserEdit,
	Filter
} from './screens'

/**
 * @author Thiago Brezinski
 *
 * Creates profile routes.
 *
 */
const Profile = StackNavigator({
		ProfileOverview: {
			screen: ProfileOverview,
			navigationOptions: {
				title: 'Perfil'
			}
		},
		ProfileEdit: {
			screen: ProfileEdit,
			navigationOptions: {
				title: 'Editar'
			}
		},
		UserEdit: {
			screen: UserEdit,
			navigationOptions: {
				title: 'Editar'
			}
		}
	},
	{
		headerMode: 'none',
	}
);

/**
 * @author Thiago Brezinski
 *
 * Creates signed-out routes.
 *
 */
export const SignedOut = StackNavigator({
	PreLogin: {
		screen: PreLogin,
		navigationOptions: {
			header: null
		}
	},
	Login: {
		screen: Login,
		navigationOptions: {
			title: 'Entrar'
		}
	},
	PreSignUp: {
		screen: PreSignUp,
		navigationOptions: {
			title: 'Cadastrar'
		}
	},
	SignUpBabysitter: {
		screen: SignUpBabysitter,
		navigationOptions: {
			title: 'Cadastro da babá'
		}
	},
	SignUpParent: {
		screen: SignUpParent,
		navigationOptions: {
			title: 'Cadastro do pai/mãe'
		}
	}
});

/**
 * @author Thiago Brezinski
 *
 * Creates signed-in tab routes.
 *
 */
export const SignedIn = TabNavigator({
		Profile: {
			screen: Profile,
			navigationOptions: {
				title: 'Perfil',
				tabBarLabel: 'Perfil',
				tabBarIcon: ({tintColor}) => (
					<Ionicons
						name={'ios-person'}
						size={30}
						style={{color: tintColor}}
					/>
				),
			},
		},
		Home: {
			screen: Home,
			navigationOptions: {
				title: 'Home',
				tabBarLabel: 'Home',
				tabBarIcon: ({tintColor}) => (
					<Ionicons
						name={'ios-home'}
						size={30}
						style={{color: tintColor}}
					/>
				),
			},
		},
		Filter: {
			screen: Filter,
			navigationOptions: {
				title: 'Filtro',
				tabBarLabel: 'Filtro',
				tabBarIcon: ({tintColor}) => (
					<Ionicons
						name={'ios-options'}
						size={30}
						style={{color: tintColor}}
					/>
				),
			},
		}
	},
	{
		initialRouteName: 'Home',
		swipeEnabled: true,
		animationEnabled: true,
		tabBarPosition: 'bottom',
	}
);

/**
 * @author Thiago Brezinski
 *
 * Redirects user to SignedIn or SignedOut routes based on the signedIn variable.
 *
 * @param signedIn indicates if the user is signed-in
 */
export const createRootNavigator = (signedIn = false) => {
	return StackNavigator({
			SignedIn: {screen: SignedIn},
			SignedOut: {screen: SignedOut}
		},
		{
			headerMode: 'none',
			mode: 'modal',
			initialRouteName: signedIn ? 'SignedIn' : 'SignedOut',
			navigationOptions: {
				gesturesEnabled: false
			}
		}
	);
};
