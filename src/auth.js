import {AsyncStorage} from 'react-native';
import moment from 'moment';

/**
 * @author Thiago Brezinski
 *
 * Requests authentication to server using login form data. If authenticated,
 * stores the token returned to AsyncStorage.
 *
 * @param values login form data
 * @returns {Promise<boolean>} true if allowed to sign in
 */
export const onSignIn = async (values) => {
	try {
		const res = await fetch('http://localhost:3000/auth/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: values.email,
				password: values.password,
			}),
		});
		if (res.ok) AsyncStorage.setItem('token', res._bodyText);
		return res.ok;
	} catch (err) {
		console.log(err);
	}
};

/**
 * @author Thiago Brezinski
 *
 * Requests user creation to server using signup form data. If valid,
 * stores the token returned to AsyncStorage.
 *
 * @param values signup form data
 * @param accountType type of user being created: Parent or Babysitter
 * @returns {Promise<boolean>} true if allowed to sign up
 */
export const onSignUp = async (values, accountType) => {
	try {
		const res = await fetch('http://localhost:3000/user', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				user: {
					email: values.email,
					password: values.password
				},
				profile: {
					name: values.name,
					surname: values.surname,
					birthDay: moment(values.birthday).format('YYYY-MM-DD')
				},
				accountType: accountType
			})
		});
		if (res.ok) AsyncStorage.setItem('token', res._bodyText);
		return res.ok;
	} catch (err) {
		console.log(err);
	}
};

/**
 * @author Thiago Brezinski
 *
 * Removes auth token from AsyncStorage when user signs-out.
 *
 * @returns {*|Promise}
 */
export const onSignOut = () => AsyncStorage.removeItem('token');

/**
 * @author Thiago Brezinski
 *
 * Tests if user is signed-in by checking if auth token exists.
 *
 * @returns {Promise<boolean>} true if signed in
 */
export const isSignedIn = async () => {
	const token = await AsyncStorage.getItem('token');
	return token !== null;
};

/**
 * @author Thiago Brezinski
 *
 * Requests for user data using auth token. If token is valid and
 * user was found, returns the json-formatted data.
 *
 * @returns {Promise<any>} user data
 */
export const getMe = async () => {
	try {
		const token = await AsyncStorage.getItem('token');
		const res = await fetch('http://localhost:3000/me', {
			method: 'GET',
			headers: {
				'Authorization': 'Bearer ' + token,
				'Content-type': 'application/json'
			}
		});
		if(res.ok) return JSON.parse(res._bodyText);
	} catch (err) {
		console.log(err);
	}
};

/**
 * @author Thiago Brezinski
 *
 * Handles facebook authentication response returned by server, either it's
 * a token or user data to store in AsyncStorage.
 *
 * @todo Enhance token-finding algorithm
 *
 * @param url returned by the server with the queries
 */
export const handleFacebookResponse = ({url}) => {
	const tokenIndex = url.indexOf('token');
	if(tokenIndex !== -1) {
		const tokenInit = tokenIndex + 6;
		const tokenEnd = url.indexOf('#');
		AsyncStorage.setItem('token', url.substring(tokenInit, tokenEnd));
	} else {
		const [, userString] = url.match(/user=([^#]+)/);
		AsyncStorage.setItem('fbuser', decodeURI(userString));
	}
};

/**
 * @author Thiago Brezinski
 *
 * Handles confirmation of user creation when signing-in for the first time
 * with Facebook. Requests user update to the server, using facebook and other
 * inputted data.
 *
 * @param values final signup form values, including what was retrieved
 * from Facebook api
 * @param accountType type of user being created: Parent or Babysitter
 * @param facebook user facebook id
 * @returns {Promise<boolean>} true if allowed to signup
 */
export const onFacebookConfirmation = async (values, accountType, facebook) => {
	try {
		const res = await fetch('http://localhost:3000/user', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				user: {
					email: values.email,
					password: values.password,
					facebook: facebook
				},
				profile: {
					name: values.name,
					surname: values.surname,
					birthDay: moment(values.birthday).format('YYYY-MM-DD')
				},
				accountType: accountType
			})
		});

		if (res.ok) AsyncStorage.setItem('token', res._bodyText);
		return res.ok;
	} catch (err) {
		console.log(err);
	}
};
