export default {

	primary: '#FFA93A',

	darker: '#111',
	dark: '#333',
	regular: '#666',
	light: '#C0C0C0',
	lighter: '#EEE',
	white: '#FFF',
	
	gradient: ['#DD5E89', '#F7BB97'],

	transparent: 'rgba(0, 0, 0, 0)',

};
