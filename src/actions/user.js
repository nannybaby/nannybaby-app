import {REQUEST_GET_ME} from '../util/actionTypes';

export function getMe() {
	return {
		type: REQUEST_GET_ME
	}
}