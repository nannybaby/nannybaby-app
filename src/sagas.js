import {takeLatest, put, all, call} from 'redux-saga/effects';
import {SUCCESS_GET_ME, REQUEST_GET_ME, FAILURE_GET_ME} from './util/actionTypes';
import {getMe} from './auth';

function* requestGetMe() {
	try {
		const res = yield call(getMe);
		// if(res) {
			yield put({type: SUCCESS_GET_ME, payload: {data: res}})
		// } else {
		// 	yield put({type: FAILURE_GET_ME})
		// }
	} catch(err) {
		yield put({type: FAILURE_GET_ME})
	}
}

export default function* root() {
	yield [
		takeLatest(REQUEST_GET_ME, requestGetMe)
	];
}
