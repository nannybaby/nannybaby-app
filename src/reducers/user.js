import {FAILURE_GET_ME, REQUEST_GET_ME, SUCCESS_GET_ME} from '../util/actionTypes';
import {INITIAL_STATE} from '../util/sagasInitialStates';

export default function user(state = INITIAL_STATE, action) {
	switch (action.type) {
		case REQUEST_GET_ME:
			return {...state, loading: true};
		case SUCCESS_GET_ME:
			return {data: action.payload.data, loading: false, error: false};
		case FAILURE_GET_ME:
			return {data: [], loading: false, error: true};
		default:
			return state;
	}
}