import React, { Component } from 'react';
import UserEditForm from '../components/UserEditForm';
import {View} from 'react-native';

export default class UserEdit extends Component {

	render() {

		return (
			<View style={styles.container}>
				<UserEditForm/>
			</View>
		);

	}
}
