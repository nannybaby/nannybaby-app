import React, { Component } from 'react';
import Btn from '../components/Btn';
import { View, Text, Button, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

export default class PreSignUp extends Component {

	render() {

		const {navigate, state} = this.props.navigation;

		return (
			<LinearGradient colors={['#DD5E89', '#F7BB97']} style={styles.container}>
				<Text style={styles.title}>Eu sou: </Text>
				<Btn btnType="btn_primary" onPress={() => navigate('SignUpParent', state.params)}>Um pai/mãe</Btn>
				<Btn btnType="btn_default" onPress={() => navigate('SignUpBabysitter', state.params)}>Uma babá</Btn>
			</LinearGradient>
		);
	}
}
const styles = StyleSheet.create({
	container: {
    flex: 1,
    padding: 15,
  },
  title: {
    fontSize: 40,
    textAlign: 'center',
    flex: 1/2,
    color: 'white',
    backgroundColor: 'transparent'
  }
});
