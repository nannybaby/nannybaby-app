import React, {Component} from 'react';
import {View} from 'react-native';
import SignUpForm from '../components/SignUpForm';
import LinearGradient from 'react-native-linear-gradient';

export default class SignUpBabysitter extends Component {

	constructor() {

		super();

		this._accountType = 'babysitter';

	}

	render() {

		const {params} = this.props.navigation.state;

		return (
			<LinearGradient colors={['#DD5E89', '#F7BB97']}>
				<SignUpForm accountType={this._accountType} facebookData={params}/>
			</LinearGradient>
		);

	}

}
