import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Btn from '../components/Btn';
import {colors} from '../styles/standards';
import LinearGradient from 'react-native-linear-gradient';

export default class PreLogin extends Component {
  render() {

    const {navigate, state} = this.props.navigation;

    return (
      <LinearGradient colors={colors.gradient} style={styles.container}>
        <Text style={styles.title}>Bem vindo!</Text>
        <Btn btnType="btn_primary" onPress={() => navigate('Login', state.params)}>Entrar</Btn>
        <Btn btnType="btn_default" onPress={() => navigate('PreSignUp', state.params)}>
          Cadastrar-se
        </Btn>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    flex: 1,
    padding: 15,
    paddingTop: '20%'
  },
  title: {
    fontSize: 40,
    textAlign: 'center',
    flex: 1/2,
    color: 'white',
    backgroundColor: 'transparent'
  },
  button: {
    height: 60,
    justifyContent: 'space-around',
    marginBottom: 20,
    borderWidth: 2,
    borderRadius: 5,
    borderColor: 'white'
	},
});
