import React, { Component } from 'react';
import {handleFacebookResponse} from '../auth';
import {openURL} from '../util/url';
import SafariView from 'react-native-safari-view';
import LoginForm from '../components/LoginForm';
import LinearGradient from 'react-native-linear-gradient';
import Btn from '../components/Btn';
import {
	Linking,
	Platform,
	AsyncStorage,
	StyleSheet
} from 'react-native';


export default class Login extends Component {

	constructor(props) {

		super(props);

		this.handleOpenFacebookURL = async ({url}) => {

			handleFacebookResponse({url});
			if (await AsyncStorage.getItem('token')) {
				props.navigation.navigate('SignedIn');
			} else {
				const user = await AsyncStorage.getItem('fbuser');

				//TODO: remove after tests
				const values = JSON.parse(user);

				props.navigation.navigate('PreSignUp', values);
			}
			if (Platform.OS === 'ios') SafariView.dismiss();
		}
		if (Platform.OS === 'ios') SafariView.dismiss();
	};

	componentDidMount() {
		Linking.addEventListener('url', this._handleOpenFacebookURL);
		Linking.getInitialURL().then((url) => {
			if (url) this._handleOpenFacebookURL(url);
		});
	};

	componentWillUnmount() {
		Linking.removeEventListener('url', this._handleOpenFacebookURL);
	};

	onFacebookSignIn = () => openURL('http://localhost:3000/auth/facebook');

	render() {
		return (
			<LinearGradient colors={['#DD5E89', '#F7BB97']} style={styles.container}>
				<Btn btnType="btn_facebook" onPress={this.onFacebookSignIn}>Entrar com o Facebook</Btn>
				<LoginForm />
			</LinearGradient>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// justifyContent: 'space-around',
		padding: 15,
	}
});
