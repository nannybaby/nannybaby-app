import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../actions/user';
import PropTypes from 'prop-types';
import styles from '../styles/_base.style';

class ProfileOverview extends Component {

	componentWillMount() {
		this.props.getMe();
	}
	
	render() {
		
		if(this.props.error || this.props.loading || this.props.data.length < 1) return null;

		const {navigate} = this.props.navigation;
		const {user} = this.props.data;
		const {profile} = user;

		return (
			<View style={styles.container}>
				<TouchableOpacity onPress={() => navigate('ProfileEdit', profile)}>
					<Text>Editar perfil</Text>
				</TouchableOpacity>
				<TouchableOpacity onPress={() => navigate('UserEdit', user)}>
					<Text>Editar informações da conta</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

const mapStateToProps = state => ({
	data: state.user.data,
	error: state.user.error,
	loading: state.user.loading
});

const mapDispatchToProps = dispatch => bindActionCreators(userActions, dispatch);

ProfileOverview.propTypes = {
	data: PropTypes.oneOfType([
		PropTypes.array,
		PropTypes.object
	]),
	error: PropTypes.bool,
	loading: PropTypes.bool,
	getMe: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileOverview);
