import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Btn from '../components/Btn';
import {colors} from '../styles/standards';
import LinearGradient from 'react-native-linear-gradient';
import {onSignOut} from '../auth';

export default class Home extends Component {

	render() {

		const {navigate} = this.props.navigation;

		return (
			<LinearGradient colors={colors.gradient} style={styles.container}>
				<View style={styles.wrapper}>
					<Text style={styles.text}>Toque para buscar uma babá</Text>
					<Btn customStyle={styles.circleBtn} />
					<Btn onPress={() => onSignOut().then(navigate('SignedOut'))}>sair</Btn>
				</View>
			</LinearGradient>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		height: '100%',
	 	flex: 1,
		justifyContent: 'space-around',
		alignItems: 'center',
		padding: 15
	},
	wrapper: {
		alignItems: 'center'
	},
	circleBtn: {
		height: 200,
		width: 200,
		borderWidth: 2,
		borderColor: 'rgba(255,255,255, 0.5)',
		borderRadius: 200,
		backgroundColor: 'transparent',
	},
	text: {
		fontSize: 20,
		color: 'white',
		backgroundColor: 'transparent',
		marginBottom: 30
	}
});
