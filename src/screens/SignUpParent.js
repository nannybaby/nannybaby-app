import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native';
import Btn from '../components/Btn';
import {colors} from '../styles/standards';
import SignUpForm from '../components/SignUpForm';
import LinearGradient from 'react-native-linear-gradient';

export default class SignUpParent extends Component {

	constructor() {
		super();
		this._accountType = 'parent';
	}

	render() {
		const {params} = this.props.navigation.state;

		return (
			<LinearGradient colors={colors.gradient} style={styles.container}>
				<ScrollView>
					<Btn btnType="btn_facebook">Cadastrar com o Facebook</Btn>
					<Text style={styles.text}>ou</Text>
					<SignUpForm accountType={this._accountType} facebookData={params}/>
				</ScrollView>
			</LinearGradient>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		padding: 20
	},
	text: {
		backgroundColor: 'transparent',
		color: 'white',
		textAlign: 'center',
		marginBottom: 20
	}
});
