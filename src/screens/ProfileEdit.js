import React, {Component} from 'react';
import moment from 'moment';
import ProfileEditForm from '../components/ProfileEditForm';
import styles from '../styles/_base.style';
import {View} from 'react-native';

export default class ProfileEdit extends Component {

	constructor(props) {

		super(props);

		const {params} = this.props.navigation.state;

		this._initialValues = {
			name: params.name,
			surname: params.surname,
			bio: params.bio,
			phone: params.phone,
			birthday: moment(params.birthday).toDate()
		};

	}

	render() {

		return (
			<View style={styles.container}>
				<ProfileEditForm initialValues={this._initialValues}/>
			</View>
		);

	}

}
