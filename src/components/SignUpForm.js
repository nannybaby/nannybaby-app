import React from 'react';
import {withNavigation} from 'react-navigation';
import {withFormik} from 'formik';
import {onFacebookConfirmation, onSignUp} from '../auth';
import {signUpInitialValues, signUpValidationSchema} from '../util/form';
import PropTypes from 'prop-types';
import {
	View,
	TouchableOpacity,
	ActivityIndicator,
	DatePickerIOS,
	Platform,
	Text,
	TextInput,
	KeyboardAvoidingView,
	StyleSheet
} from 'react-native';
import Btn from '../components/Btn';
import DatePicker from 'react-native-datepicker'

const SignUpForm = ({values, touched, errors, isSubmitting, setFieldValue, handleSubmit}) => {
	return (
	<KeyboardAvoidingView behavior="padding" style={styles.formContainer}>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			placeholder='Email'
			keyboardType="email-address"
			value={values.email}
			onChangeText={text => setFieldValue('email', text)}
		/>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			secureTextEntry
			placeholder='Senha'
			value={values.password}
			onChangeText={text => setFieldValue('password', text)}
		/>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			secureTextEntry
			placeholder='Confirme a senha'
			value={values.passwordConfirmation}
			onChangeText={text => setFieldValue('passwordConfirmation', text)}
		/>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			placeholder='Nome'
			value={values.name}
			onChangeText={text => setFieldValue('name', text)}
		/>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			placeholder='Sobrenome'
			value={values.surname}
			onChangeText={text => setFieldValue('surname', text)}
		/>
		<DatePicker
			style={{width: '90%'}}
			mode="date"
      placeholder="select date"
      format="YYYY-MM-DD"
			confirmBtnText="Confirm"
        cancelBtnText="Cancel">
			</DatePicker>
		{/* <TouchableOpacity
			onPress={this.toogleDatePicker}
			style={[styles.input, styles.datePickerPlaceholder]}>
		</TouchableOpacity> */}
		{/* DatePicker boladão */}
		<Btn onPress={handleSubmit} btnType="btn_primary">
			Enviar
		</Btn>
		{isSubmitting && <ActivityIndicator/>}
		{errors.message && <Text>{errors.message}</Text>}
		{touched.email && errors.email && <Text>{errors.email}</Text>}
		{touched.password && errors.password && <Text>{errors.password}</Text>}
		{touched.passwordConfirmation && errors.passwordConfirmation &&
		<Text>{errors.passwordConfirmation}</Text>}
		{touched.name && errors.name && <Text>{errors.name}</Text>}
		{touched.surname && errors.surname && <Text>{errors.surname}</Text>}
		{touched.birthday && errors.birthday && <Text>{errors.birthday}</Text>}
	</KeyboardAvoidingView>)
};

SignUpForm.propTypes = {
	accountType: PropTypes.string.isRequired,
	facebookData: PropTypes.shape({
		email: PropTypes.string,
		facebook: PropTypes.string
		//TODO: Add the rest
	})
};

export default withNavigation(
	withFormik({
		mapPropsToValues: ({facebookData}) => facebookData ? facebookData : signUpInitialValues,
		validationSchema: signUpValidationSchema,
		handleSubmit: (values, {props, setSubmitting, setErrors}) => {
			if (props.facebookData) {
				onFacebookConfirmation(values, props.accountType, props.facebookData.facebook)
				.then(allowed => {
					if (allowed) {
						props.navigation.navigate('SignedIn');
					} else {
						setErrors({message: 'Erro no cadastro'})
					}
				})
				.then(setSubmitting(false))
				.catch(err => {
					setSubmitting(false);
					setErrors({message: err.message});
				})
			} else {
				onSignUp(values, props.accountType)
				.then(allowed => {
					if (allowed) {
						props.navigation.navigate('SignedIn');
					} else {
						setErrors({message: 'Erro no cadastro'});
					}
				})
				.then(setSubmitting(false))
				.catch(err => {
					setSubmitting(false);
					setErrors({message: err.message});
				});
			}
		}
	})(SignUpForm)
);

const styles = StyleSheet.create({
	input: {
		height: 50,
		fontSize: 20,
		marginBottom: 10,
		borderRadius: 5,
		backgroundColor: 'rgba(255, 255, 255, 0.2)',
		padding: 10
	},
	text: {
		backgroundColor: 'transparent',
		color: 'white',
		textAlign: 'center',
		marginBottom: 20
	},
	datePickerPlaceholder: {
		opacity: 0.5
	}
});
