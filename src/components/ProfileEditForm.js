import React from 'react';
import {withNavigation} from 'react-navigation';
import {withFormik} from 'formik';
import {profileEditValidationSchema} from '../util/form';
import PropTypes from 'prop-types';
import {
	ActivityIndicator,
	Button,
	DatePickerIOS,
	KeyboardAvoidingView,
	Platform,
	Text,
	TextInput
} from 'react-native';

//TODO: Edit photo
const ProfileEditForm = ({values, touched, errors, isSubmitting, setFieldValue, handleSubmit}) => (
	<KeyboardAvoidingView behavior="padding">
		<Text>Nome</Text>
		<TextInput
			returnKeyType="next"
			placeholder='Digite o nome'
			value={values.name}
			onChangeText={text => setFieldValue('name', text)}
		/>
		<Text>Sobrenome</Text>
		<TextInput
			returnKeyType="next"
			placeholder='Digite a sobrenome'
			value={values.surname}
			onChangeText={text => setFieldValue('surname', text)}
		/>
		<Text>Biografia</Text>
		<TextInput
			returnKeyType="next"
			placeholder='Digite o biografia'
			value={values.bio}
			onChangeText={text => setFieldValue('bio', text)}
		/>
		<Text>Telefone</Text>
		<TextInput
			returnKeyType="go"
			placeholder='Digite o telefone'
			value={values.phone}
			onChangeText={text => setFieldValue('phone', text)}
		/>
		<Text>Data de nascimento</Text>
		{Platform.OS === 'ios'
			?
			<DatePickerIOS
				date={values.birthday}
				onDateChange={date => setFieldValue('birthday', date)}
				mode="date"
			/>
			:
			<Text>Implementar DatePickerAndroid</Text>
		}
		<Button
			onPress={handleSubmit}
			title='Confirmar'
		/>
		{isSubmitting && <ActivityIndicator/>}
		{errors.message && <Text>{errors.message}</Text>}
		{touched.name && errors.name && <Text>{errors.name}</Text>}
		{touched.surname && errors.surname && <Text>{errors.surname}</Text>}
		{touched.bio && errors.bio && <Text>{errors.bio}</Text>}
		{touched.phone && errors.phone && <Text>{errors.phone}</Text>}
		{touched.birthday && errors.birthday && <Text>{errors.birthday}</Text>}
	</KeyboardAvoidingView>
);

ProfileEditForm.propTypes = {
	initialValues: PropTypes.shape({
		name: PropTypes.string.isRequired,
		surname: PropTypes.string.isRequired,
		bio: PropTypes.string,
		phone: PropTypes.string,
		birthday: PropTypes.instanceOf(Date)
	})
};

export default withNavigation(
	withFormik({
		mapPropsToValues: ({initialValues}) => initialValues,
		validationSchema: profileEditValidationSchema,
		handleSubmit: (values, {props, setSubmitting, setErrors}) => {
			//TODO: Handle edit request
		}
	})(ProfileEditForm)
);
