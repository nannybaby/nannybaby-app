import React from 'react';
import { withNavigation } from 'react-navigation';
import { withFormik } from 'formik';
import {loginInitialValues, loginValidationSchema} from '../util/form';
import {
	ActivityIndicator,
	KeyboardAvoidingView,
	Text,
	TextInput,
	StyleSheet
} from 'react-native';
import {onSignIn} from '../auth';
import Btn from './Btn'

const LoginForm = ({values, touched, errors, isSubmitting, setFieldValue, handleSubmit}) => (
	<KeyboardAvoidingView style={styles.formContainer} behavior="padding">
		<Text style={styles.text}>ou</Text>
		<TextInput
			style={styles.input}
			placeholder='Digite o e-mail'
			returnKeyType="next"
			autoCapitalize='none'
			autoCorrect={false}
			value={values.email}
			onChangeText={text => setFieldValue('email', text)}
		/>
		<TextInput
			style={styles.input}
			secureTextEntry
			placeholder='Digite a senha'
			returnKeyType="go"
			value={values.password}
			onChangeText={text => setFieldValue('password', text)}
		/>
		<Btn onPress={handleSubmit} btnType="btn_primary">
			Entrar
		</Btn>
		{isSubmitting && <ActivityIndicator/>}
		{errors.message && <Text style={[styles.text, styles.error]}>{errors.message}</Text>}
		{touched.email && errors.email && <Text style={[styles.text, styles.error]}>{errors.email}</Text>}
		{touched.password && errors.password && <Text style={[styles.text, styles.error]}>{errors.password}</Text>}
	</KeyboardAvoidingView>
);

export default withNavigation(
	withFormik({
		// mapPropsToValues: () => loginInitialValues, Versão correta
		//Valores provisórios para facilitar o desenvolvimento
		mapPropsToValues: () => ({email: 'teste@teste.com', password: 'oritei'}),
		validationSchema: loginValidationSchema,
		handleSubmit: (values, {props, setSubmitting, setErrors}) => {
			onSignIn(values)
				.then(allowed => {
					if(allowed) {
						props.navigation.navigate('SignedIn');
					} else {
						setErrors({message: 'Usuário ou senha inválidos'})
					}
				})
				.then(setSubmitting(false))
				.catch(err => {
					setSubmitting(false);
					setErrors({message: err.message});
				});
		}
	})(LoginForm)
);

export const styles = StyleSheet.create({
	input: {
		height: 50,
		fontSize: 20,
		marginBottom: 10,
		borderRadius: 5,
		backgroundColor: 'rgba(255, 255, 255, 0.2)',
		padding: 10
	},
	button: {
		backgroundColor: 'transparent',
		color: 'red'
	},
	text: {
		backgroundColor: 'transparent',
		color: 'white',
		textAlign: 'center',
		marginBottom: 20
	},
	error: {
		color: 'red',
		fontSize: 20
	}
})
