import React from 'react';
import PropTypes from 'prop-types';
import {
	TouchableOpacity,
	Text,
	StyleSheet,
} from 'react-native';

const Btn = ({customStyle, btnType, onPress, children}) => {
	
	const buttonStyles = customStyle ? customStyle : [styles.button, styles[btnType]];
	
	return (
		<TouchableOpacity onPress={onPress}
											style={buttonStyles}>
			<Text style={styles.text}>
				{children}
			</Text>
		</TouchableOpacity>
	);
};

Btn.propTypes = {
	btnType: PropTypes.string,
	onPress: PropTypes.func,
	//TODO: customStyle: supplied `number` ?
	// customStyle: PropTypes.object
};

const styles = StyleSheet.create({
	button: {
		height: 60,
		justifyContent: 'space-around',
		marginBottom: 20,
		borderWidth: 2,
		borderRadius: 5,
		borderColor: 'white'
	},
	text: {
		fontSize: 20,
		textAlign: 'center',
		textAlignVertical: 'center',
		color: 'white',
	},
	btn_facebook: {
		backgroundColor: '#4267b2',
		borderColor: '#4267b2'
	},
	btn_default: {
		backgroundColor: '#9F84BD',
		borderColor: '#9F84BD'
	},
	btn_primary: {
		backgroundColor: '#BCB6FF',
		borderColor: '#BCB6FF'
	}
});

export default Btn;