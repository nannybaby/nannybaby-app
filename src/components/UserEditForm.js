import React from 'react';
import {withNavigation} from 'react-navigation';
import {withFormik} from 'formik';
import {userEditInitialValues, userEditValidationSchema} from '../util/form';
import {
	ActivityIndicator,
	Button,
	KeyboardAvoidingView,
	Text,
	TextInput
} from 'react-native';

const UserEditForm = ({values, touched, errors, isSubmitting, setFieldValue, handleSubmit}) => (
	<KeyboardAvoidingView behavior="padding">
		<Text>Senha</Text>
		<TextInput
			style={styles.input}
			returnKeyType="next"
			secureTextEntry
			placeholder='Digite a senha'
			value={values.password}
			onChangeText={text => setFieldValue('password', text)}
		/>
		<Text>Confirmação de senha</Text>
		<TextInput
			style={styles.input}
			returnKeyType="go"
			secureTextEntry
			placeholder='Digite a senha'
			value={values.passwordConfirmation}
			onChangeText={text => setFieldValue('passwordConfirmation', text)}
		/>
		<Button
			onPress={handleSubmit}
			title='Confirmar'
		/>
		{isSubmitting && <ActivityIndicator/>}
		{touched.password && errors.password && <Text>{errors.password}</Text>}
		{touched.passwordConfirmation && errors.passwordConfirmation && <Text>{errors.passwordConfirmation}</Text>}
	</KeyboardAvoidingView>
);

export default withNavigation(
	withFormik({
		mapPropsToValues: () => userEditInitialValues,
		validationSchema: userEditValidationSchema,
		handleSubmit: (values, {props, setSubmitting, setErrors}) => {
			//TODO: Handle edit request
		}
	})(UserEditForm)
);
